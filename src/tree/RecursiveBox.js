import React, { useRef, useState } from 'react'
import { radians } from '../utils/math'

let count = 0
Object.assign(window, { getBoxCount: () => count })

export function RecursiveBox({ height = 3, size = .5, childCount = 0, ...props }) {

  const [id, setId] = useState(-1)
  if (id === -1)
    setId(count++)

  const group = useRef()
  const [hover, setHover] = useState(false)
  const [{ innerChildCount }, setState] = useState({ innerChildCount: childCount })

  if (childCount > innerChildCount) {
    setState({ innerChildCount: childCount })
  }

  const onClick = () => {
    setState({
      innerChildCount: innerChildCount + 1,
    })
  }

  return (
    <group {...props} ref={group}>
      <mesh position={[0, height / 2, 0]}
        onPointerOver={() => setHover(true)}
        onPointerOut={() => setHover(false)}
        onClick={onClick}>
        <boxBufferGeometry args={[size, height, size]} />
        <meshPhysicalMaterial color={hover ? 'hotpink' : '#ffcc00'} />
      </mesh>
      {(childCount > 0 || innerChildCount > 0) && (
        <>
          <RecursiveBox
            childCount={Math.max(childCount, innerChildCount) - 1}
            position={[0, height, 0]}
            rotation={radians(3, 45, 10)}
            scale={[.9, .9, .9]} />
          <RecursiveBox
            childCount={Math.max(childCount, innerChildCount) - 1}
            position={[0, height, 0]}
            rotation={radians(-2, 35, -10)}
            scale={[.8, .8, .8]} />
        </>
      )}
    </group>
  )
}
