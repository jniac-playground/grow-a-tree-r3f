import React, { useEffect, useRef, useState } from 'react'
import { useThree } from 'react-three-fiber'
import { BoxBufferGeometry, BufferAttribute, Color, Euler, Matrix4, MeshPhysicalMaterial, Quaternion, Vector3 } from 'three'
import { enumerate, radians } from '../utils/math'

/**
 * NOTE:
 *  - improvement: remove "matrices" in state, replace with a floatArray (no data redundancy)?
 */

const geometry = new BoxBufferGeometry(0.5, 3, 0.5)
const material = new MeshPhysicalMaterial({ color:'pink' })

const getBranch = ({
  x = 0,
  y = 0,
  z = 0,
  rx = 0,
  ry = 0,
  rz = 0,
  scale = 1,
}) => {
  const p = new Vector3(x, y + 1.5, z)
  const r = new Quaternion().setFromEuler(new Euler(...radians(rx, ry, rz)))
  const s = new Vector3(scale, scale, scale)
  const m = new Matrix4()
  return m.compose(p, r, s).multiply(new Matrix4().makeTranslation(0, 1.5, 0))
}

/**
 * From an index, get all child indexes (including the given index).
 * @param {Map<number, number[]>} graph 
 * @param {number} parentId 
 * @returns {Set<number>}
 */
const getChildrenIndexes = (graph, parentId) => {
  const children = new Set()
  if (parentId === -1)
    return children
  const current = [parentId]
  while (current.length > 0) {
    const id = current.pop()
    children.add(id)
    const childIds = graph.get(id)
    if (childIds) {
      current.push(...childIds)
    }
  }
  return children
}

/**
 * From an index, get all child indexes (including the given index).
 * @param {Map<number, number[]>} graph 
 * @param {number} parentId 
 * @returns {Set<number>}
 */
const getTipIndexes = (graph, parentId) => {
  const tips = new Set()
  const current = [parentId]
  while (current.length > 0) {
    const id = current.pop()
    const childIds = graph.get(id)
    if (childIds) {
      current.push(...childIds)
    } else {
      tips.add(id)
    }
  }
  return tips
}

/**
 * @returns {{
 *  matrices:Float32Array
 *  graph:Map<number,number[]>
 * }}
 */
const getInitialState = () => ({
  matrices: new Float32Array(new Matrix4().makeTranslation(0, 1.5, 0).elements),
  graph: new Map(),
})

export const InstancedBox = (props) => {

  const mesh = useRef(null)
  const [shift, setShift] = useState(false)
  useEffect(() => {
    const onDown = e => (e.key === 'Shift') && setShift(true)
    const onUp = e => (e.key === 'Shift') && setShift(false)
    window.addEventListener('keydown', onDown)
    window.addEventListener('keyup', onUp)
    return () => {
      window.removeEventListener('keydown', onDown)
      window.removeEventListener('keyup', onUp)
    }
  })

  const getColorArray = (instanceId) => {
    const children = getChildrenIndexes(graph, instanceId)
    const colorNormal = new Color('white')
    const colorAdd = new Color('#0c6')
    const colorRemove = new Color('#f33')
    const count = matrices.length / 16
    const array = new Float32Array(count * 3)
    for (const index of enumerate(count)) {
      const color = children.has(index) ? (shift ? colorRemove : colorAdd) : colorNormal
      array[index * 3 + 0] = color.r
      array[index * 3 + 1] = color.g
      array[index * 3 + 2] = color.b
    }
    return array
  }

  const { invalidate } = useThree()

  const [hoverId, setHoverId] = useState(-1)
  let [{ matrices, graph }, setState] = useState(getInitialState)

  // initialize (componentDidMount)
  useEffect(() => {
    const count = matrices.length / 16
    mesh.current.count = count
    mesh.current.instanceMatrix.array = matrices
    mesh.current.instanceMatrix.needsUpdate = true

    if (mesh.current.instanceColor) {
      mesh.current.instanceColor.array = getColorArray(hoverId)
    } else {
      mesh.current.instanceColor = new BufferAttribute(getColorArray(hoverId), 3)
    }
    mesh.current.instanceColor.needsUpdate = true
    invalidate()
  })

  
  /**
   * Get the new matrices, for a given id.
   * Here is the design of the tree.
   * @param {number} id 
   */
  const getNewMatrices = (id) => {
    // local matrices
    const m1 = getBranch({ rx:3, ry:45, rz:20, scale:.9 })
    const m2 = getBranch({ rx:-2, ry:35, rz:-20, scale:.8 })
    // the compute "tree" matrices
    const m = new Matrix4()
    mesh.current.getMatrixAt(id, m)
    return [
      m.clone().multiply(m1),
      m.clone().multiply(m2),
    ]
  }

  /**
   * Push new matrices elements into the buffer.
   * @param {Matrix4[]} newMatrices 
   */
  const pushNewMatrices = (newMatrices) => {
    const oldMatrices = matrices
    const oldCount = matrices.length / 16
    matrices = new Float32Array((oldCount + newMatrices.length) * 16)
    matrices.set(oldMatrices, 0)
    for (const [index, matrix] of newMatrices.entries()) {
      matrices.set(matrix.elements, (oldCount + index) * 16)
    }
  }

  /**
   * Grow from an id (instanceId).
   * @param {number} instanceId 
   */
  const grow = (instanceId) => {
    let count = matrices.length / 16
    const newMatrices = []
    for (const id of getTipIndexes(graph, instanceId)) {
      const newMatricesIds = []
      for (const matrix of getNewMatrices(id)) {
        newMatricesIds.push(count)
        newMatrices.push(matrix)
        count++
      }
      graph.set(id, newMatricesIds)
    }
    pushNewMatrices(newMatrices)
  }

  
  const prune = (instanceId) => {
    const removed = getChildrenIndexes(graph, instanceId)
    const oldMatrices = matrices
    const oldCount = oldMatrices.length / 16
    const count = (oldMatrices.length / 16) - removed.size
    matrices = new Float32Array(count * 16)
    const oldToNew = new Map()
    // Looping over all matrices/nodes.
    // Removed indexes are removed from graph (delete).
    // "instanceId" is removed from the children array (that hold the value).
    // "matrices" is rebuilt from the kept indexes. 
    let index = 0
    for (const oldIndex of enumerate(oldCount)) {
      if (removed.has(oldIndex)) {
        graph.delete(oldIndex)
      } else {
        const children = graph.get(oldIndex)
        if (children && children.includes(instanceId)) {
          graph.set(oldIndex, children.filter(id => id !== instanceId))
        }
        oldToNew.set(oldIndex, index)
        matrices.set(oldMatrices.subarray(oldIndex * 16, (oldIndex + 1) * 16), index * 16)
        index += 1
      }
    }
    // Rebuild graph, from the old one, using the "oldToNew" index map.
    const oldGraph = graph
    graph = new Map()
    for (const [id, children] of oldGraph.entries()) {
      graph.set(oldToNew.get(id), children.map(child => oldToNew.get(child)))
    }
  }

  const onClick = (instanceId) => {

    if (shift) {
      prune(instanceId)
    } else {
      grow(instanceId)
    }

    const count = matrices.length / 16
    mesh.current.count = count
    mesh.current.instanceMatrix = new BufferAttribute(matrices, 16)
    mesh.current.instanceColor = new BufferAttribute(new Float32Array(count * 3), 3)
    mesh.current.instanceMatrix.needsUpdate = true

    setState({ matrices, graph })
    invalidate()
  }

  return (
    <instancedMesh
      {...props}
      ref={mesh}
      onPointerOver={event => setHoverId(event.intersections[0].instanceId)}
      onPointerMove={event => setHoverId(event.intersections[0].instanceId)}
      onPointerOut={() => setHoverId(-1)}
      onClick={event => onClick(event.intersections[0].instanceId)}
      args={[geometry, material, 0]}
    />
  )
}