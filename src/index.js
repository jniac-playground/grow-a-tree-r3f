import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './index.css'
import * as THREE from 'three'

ReactDOM.render(<App />, document.getElementById('root'))

Object.assign(window, { THREE })
