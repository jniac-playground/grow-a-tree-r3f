import { OrbitControls } from '@react-three/drei'
import React, { useEffect, useRef } from 'react'
import { Canvas, useFrame, useThree } from 'react-three-fiber'
import { InstancedBox } from './tree/InstancedBox'
import { RecursiveBox } from './tree/RecursiveBox'
import { radians } from './utils/math'

function Camera(props) {
  const ref = useRef()
  const { setDefaultCamera } = useThree()
  // Make the camera known to the system
  useEffect(() => {
    setDefaultCamera(ref.current)
  }, [setDefaultCamera])
  // Update it every frame
  useFrame(() => ref.current.updateMatrixWorld())
  return <perspectiveCamera ref={ref} {...props} />
}

const TreeSpot = ({ children, ...props }) => {

  return (
    <group {...props}>
      <mesh rotation={radians(-90, 0, 0)}>
        <circleBufferGeometry args={[5, 64]}/>
        <meshLambertMaterial transparent opacity={.1}/>
      </mesh>
      {children}
    </group>
  )
}

function App() {

  return (
    <Canvas invalidateFrameloop>
      <Camera position={[0, 6, 12]}/>
      <OrbitControls dampingFactor={1}/>
      <ambientLight intensity={0.5}/>
      <spotLight position={[20, 30, 10]}/>
      <TreeSpot position={[14, 0, 0]}>
        <RecursiveBox/>
      </TreeSpot>
      <TreeSpot position={[0, 0, 0]}>
        <InstancedBox/>
      </TreeSpot>
    </Canvas>
  )
}

export default App
